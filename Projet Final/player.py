import pygame

# Création du Joueur / Class du Joueur
class Player(pygame.sprite.Sprite):

    def __init__(self):

        super().__init__()
        self.velocity = 5
        self.image = pygame.image.load("assets/player.webp")
        self.rect = self.image.get_rect()
        self.rect.x = 150
        self.rect.y = 525

    def right(self):

        self.rect.x += self.velocity

    def left(self):

        self.rect.x -= self.velocity

    def jump(self):

        self.rect.y -= self.velocity