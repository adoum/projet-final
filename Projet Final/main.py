import pygame
import math
from game import Game
pygame.init()

# Création de la fenêtre
pygame.display.set_caption("Game")
screen = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)
background = pygame.image.load("assets/bg.png")

music = pygame.mixer.music.load("assets/music.mp3")

button = pygame.image.load("assets/button.webp")
button = pygame.transform.scale(button, (250, 250))
button_rect = button.get_rect()
button_rect.x = math.ceil(screen.get_width() / 3)
button_rect.y = math.ceil(screen.get_height() / 2)

banner = pygame.image.load("assets/banner.png")

game = Game()

turn = True

while turn:

    screen.blit(background, (0, 0))

    if game.is_playing:
        game.update(screen)
    else:
        screen.blit(banner, (390, 47))
        screen.blit(button, (500, 400))

    pygame.display.flip()
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            turn = False
            pygame.quit()
        elif event.type == pygame.KEYDOWN:
            game.pressed[event.key] = True
        elif event.type == pygame.KEYUP:
            game.pressed[event.key] = False
        elif event.type == pygame.MOUSEBUTTONDOWN:
            if button_rect.collidepoint(event.pos):
                game.is_playing = True
                pygame.mixer.music.play()

