import pygame
from player import Player

# Class principal du Jeu
class Game:

    def __init__(self):

        self.is_playing = False
        self.player = Player()
        self.pressed = {}

    def update(self, screen):

        screen.blit(self.player.image, self.player.rect)

        if self.pressed.get(pygame.K_RIGHT) or self.pressed.get(pygame.K_d) and self.player.rect.x < 1190:
            self.player.right()
        elif self.pressed.get(pygame.K_LEFT) or self.pressed.get(pygame.K_q) and self.player.rect.x > -10:
            self.player.left()
